<?php
header('Content-type:text/html;charset=utf-8');
session_start();
if(isset($_SESSION['name'])){
    session_unset($_SESSION['name']);
    session_destroy();
    setcookie(session_name(),'');
    echo "<script>alert('退出成功！');location.href='login.php';</script>";
}else{
    echo "<script>alert('退出失败！');</script>";
}
?>
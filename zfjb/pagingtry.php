<?php
header('content-type:text/html;charset=utf-8');
    include('conn.php');
    $sql="select * from persons order by id desc";
    $result=mysql_query($sql);
    $num=mysql_num_rows($result);                   //页总数
    $pageSize=5;                                    //页尺寸
    $pageCount=ceil($num/$pageSize);                //总页数
    $pageNo=isset($_GET['page'])?$_GET['page']:1;   //页码
    $pageNext=$pageNo+1;                            //下一页
    $pagePrev=$pageNo-1;                            //上一页
     
    //判断页码越界
    if($pageNext>$pageCount)  $pageNext=$pageCount;
    if($pagePrev<1)        $pagePrev=1;
    if($pageNo>$pageCount)    $pageNo=$pageCount;
    if($pageNo<1)              $pageNo=1;
 
    $offset=($pageNo-1)*$pageSize;                  //偏移量 
    mysql_data_seek($result, $offset);              //将结果指针移至offset处
?>
 
<style type="text/css">
    tr{background-color:#ffffff; font-size:12px;}
    td{text-align:center;height:30px;}
 
</style>
<table width="100%;" bgcolor="#333666" cellspacing="1">
    <tr>
        <td>ID</td>
        <td>姓名</td>
        <td>权限</td>
    </tr>
    <tr>
    <?php
     
    for ($i=0; $i <$pageSize ; $i++):
        $row=mysql_fetch_assoc($result);
         if ($row) :
?>
        <td><?php echo $row['id']?></td>
        <td><?php echo $row['name']?></td>
        <td>
                <?php
 
                switch ($row['font']) {
                case '1':
                    echo "老师";
                    break;
                case '2':
                    echo "教授";
                    break;
                 
                default:
                    echo "学生";
                    break;
            }
 
 
        ?>
</td>
    </tr>
 
    <?php
        endif;
    endfor;
    ?>
    <tr>
        <td colspan="3">
            共<?php echo $num ?>条
            共<?php echo $pageCount ?> 页
            每页 <?php echo $pageSize ?>条
            <a href="pagingtry.php?page=1">首页</a>
            <a href="pagingtry.php?page=<?php echo $pagePrev ?> ">第一页</a>
            <a href="pagingtry.php?page=<?php echo $pageNext?>">下一页</a>
            <a href="pagingtry.php?page=<?php echo $pageCount?>">最后一页</a>
 
        </td>
    </tr>
 
</table>